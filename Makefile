bin/matmul: obj/matriz.o 
	gcc -Wall obj/matriz.o -o bin/matmul -fsanitize=address,undefined
obj/matriz.o: src/matriz.c
	gcc -Wall -pthread -c src/matriz.c -o obj/matriz.o 
.PHONY: clean
clean:
	rm bin/matmul obj/*
